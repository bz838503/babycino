class TestBugH2 {
    public static void main(String[] a) {
	System.out.println(new Test().f());
    }
}

class Test {

    public int f() {
	int result;
	int count;
	boolean done;
	result = 0;
	count = 1;
	done = true;
	while (!done) { // Runtime error: Essentially a while false loop, should be a do-while loop instead.
	    result = result + count;
	    count = count + 1;
	    done = (10 < count); // Should be 10 > count to get the expected output, or count < 10 since ">" is not a valid operator in the compiler.
	}
	return result;
    }

}

// Will compile fine but output isn't as expected due to a mistake in logic.

